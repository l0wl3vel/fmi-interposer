# Interposer for FMI 2.0.2

This interposer was built to insert custom code into binary FMUs. The primary motivation is packaging NeuralFMUs.

This example builds a NeuralFMU using a Neural Network on the derivative of the state vector.

## Features
- Augment any function call of FMUs
- Add additional state to fmuComponents
- No direct dependency to the FMU being packaged
- Cross Platform and easy™ to cross compile (tested on linux host. No reason windows should not work as hosts)


## Dependencies

- rustup
- zip and unzip in PATH

## Used libraries
- Tract for NN Inference
- opaque_pointer for type replacement of fmuComponent
- bindgen for FFI code generation

## Setup

Make sure that the mingw toolchain for the system to cross compile is installed. The native toolchain should be fine either way.

```bash
${PACKAGE_MANAGER} install mingw-w64-bzip2 mingw-w64-gcc
```

The Rust Cross Compilation Tool chains for win64 and linux64 needs to be installed:

```bash
$ rustup toolchain install stable-x86_64-pc-windows-gnu
$ rustup toolchain install stable-x86_64-unknown-linux-gnu

$ rustup target add x86_64-pc-windows-gnu
$ rustup target add x86_64-unknown-linux-gnu
```

## Usage

1. Place a FMU in the root folder and rename it to `${FMU_NAME}.fmu_orig`.
2. Place the pre-trained `${FMU_NAME}.onnx` file in the root folder
3. Change the `{FMU_NAME}` in `build.sh` and `build.rs`
4. Implement your custom functionality in `lib.rs`
5. Run `./build.sh`
6. Enjoy your NeuralFMU `${FMU_NAME}.fmu`

## Limitations
- Win32 interposers are currently not built. It may be possible to enable the build and package them. Just take a look at the build_release.sh script