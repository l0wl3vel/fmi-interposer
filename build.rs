extern crate bindgen;

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

const FMU_NAME: &'static str = "SpringPendulum1D";

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    //cc::Build::new().file("src/logger.c").compile("liblogger.a");

    let bindings_path = "src/bindings/".to_string();

    std::fs::remove_file(bindings_path.clone() + "internal_bindings.rs").unwrap_or_default();
    std::fs::remove_file(bindings_path.clone() + "external_bindings.rs").unwrap_or_default();
    std::fs::remove_file(bindings_path.clone() + "external_type_bindings.rs").unwrap_or_default();
    std::fs::remove_file("src/name_helper.rs").unwrap_or_default();

    let NAME_HELPER = format!(
        "
pub const FMU_NAME: &str = \"{}\";
#[macro_export]
macro_rules! ONNX_BYTES {{
    () => {{
        include_bytes!(\"../{}.onnx\");
    }};
}}
",
        FMU_NAME, FMU_NAME
    );

    let mut name_helper_file = File::create("src/name_helper.rs").unwrap();

    name_helper_file.write_all(NAME_HELPER.as_bytes()).unwrap();

    bindgen_complete_setup()
        .dynamic_library_name("fmi2")
        .generate()
        .expect("Unable to generate internal bindings")
        .write_to_file(PathBuf::from(bindings_path.clone()).join("internal_bindings.rs"))
        .expect("Couldn't write bindings!");

    bindgen_complete_setup()
        .generate()
        .expect("Unable to generate external bindings")
        .write_to_file(PathBuf::from(bindings_path.clone()).join("external_bindings.rs"))
        .expect("Couldn't write bindings!");

    bindgen_just_types_setup()
        .generate()
        .expect("Unable to generate type bindings")
        .write_to_file(PathBuf::from(bindings_path.clone()).join("external_type_bindings.rs"))
        .expect("Couldn't write bindings!");

    println!("cargo:rustc-link-lib=bz2");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=include/fmi2Functions.h");
    /*
    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("include/fmi2Functions.h")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .dynamic_library_name("FMI2")
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from("src");
    bindings
        .write_to_file(out_path.join("fmu_bindings.rs"))
        .expect("Couldn't write bindings!"); */
}

fn bindgen_setup() -> bindgen::Builder {
    let mut builder = bindgen::Builder::default()
        .header("include/fmi2Functions.h")
        .layout_tests(false)
        .blocklist_type(".*TYPE") //Disable all Function Pointer Types
        .default_enum_style(bindgen::EnumVariation::Consts);
    builder
}

fn bindgen_complete_setup() -> bindgen::Builder {
    bindgen_setup()
        .allowlist_function("fmi2.*")
        .allowlist_type("fmi2.*")
        .allowlist_var("fmi2.*")
}

fn bindgen_just_types_setup() -> bindgen::Builder {
    bindgen_setup().allowlist_type("fmi2.*")
}
