
pub const FMU_NAME: &str = "SpringPendulum1D";
#[macro_export]
macro_rules! ONNX_BYTES {
    () => {
        include_bytes!("../SpringPendulum1D.onnx");
    };
}
