use crate::bindings::{self, external_type_bindings, internal_bindings};
use lazy_static::lazy_static;
use std::{borrow::BorrowMut, collections::HashMap};

#[repr(C)]
pub struct FmuComponent {
    pub internal: internal_bindings::fmi2Component,
    pub test: Box<f64>,
}

/* lazy_static! {
    static ref instances: HashMap<external_type_bindings::fmi2Component, FmuComponent> =
        HashMap::new();
} */

impl FmuComponent {
    pub fn new(internal: internal_bindings::fmi2Component) -> FmuComponent {
        FmuComponent {
            internal,
            test: Box::new(420.0),
        }
    }
    pub fn get_internal_fmu(&self) -> &internal_bindings::fmi2Component {
        &self.internal
    }
}

/* pub fn get_from_opaque(
    mut opaque_pointer: internal_bindings::fmi2Component,
) -> internal_bindings::fmi2Component {
    unsafe { instances.get(opaque_pointer.borrow_mut()).unwrap().internal }
}
 */
