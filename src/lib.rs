mod bindings;
mod neural_networks;

use bindings::external_bindings::*;
use bindings::external_type_bindings::fmi2Component;
use bindings::internal_bindings;

mod fmu_component_container;

use fmu_component_container::FmuComponent;
use tract_onnx::prelude::*;
use tract_onnx::tract_hir::tract_num_traits::Num;

use std::io::Cursor;
use std::path::PathBuf;

use lazy_static::lazy_static;

lazy_static! {
    static ref INTERNAL_FMU: internal_bindings::fmi2 = initialize_internal_bindings();
//static ref onnx_model_container: onnx_container::OnnxContainer = initialize_onnx_container();
}

fn get_os_library_extension() -> &'static str {
    if cfg!(target_os = "windows") {
        "dll"
    } else if cfg!(target_os = "macos") {
        "dylib"
    } else {
        "so"
    }
}

fn initialize_internal_bindings() -> internal_bindings::fmi2 {
    let mut path_buf = whereami::module_path().unwrap();
    PathBuf::pop(&mut path_buf);
    let path = String::from(path_buf.to_str().unwrap());
    println!("{}", path);

    path_buf.push(format!(
        "{}_internal.{}",
        neural_networks::FMU_NAME,
        get_os_library_extension()
    ));

    unsafe { internal_bindings::fmi2::new(path_buf) }.unwrap()
}

#[no_mangle]
extern "C" fn fmi2GetTypesPlatform() -> *const ::std::os::raw::c_char {
    unsafe { INTERNAL_FMU.fmi2GetTypesPlatform() }
}

#[no_mangle]
extern "C" fn fmi2GetVersion() -> *const ::std::os::raw::c_char {
    unsafe { INTERNAL_FMU.fmi2GetVersion() }
}

#[no_mangle]
extern "C" fn fmi2SetDebugLogging(
    c: *mut FmuComponent,
    loggingOn: fmi2Boolean,
    nCategories: size_t,
    categories: *const fmi2String,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetDebugLogging(
            opaque_pointer::mut_object(c).unwrap().internal,
            loggingOn,
            nCategories,
            categories,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2Instantiate(
    instanceName: fmi2String,
    fmuType: fmi2Type,
    fmuGUID: fmi2String,
    fmuResourceLocation: fmi2String,
    functions: *const internal_bindings::fmi2CallbackFunctions,
    visible: fmi2Boolean,
    loggingOn: fmi2Boolean,
) -> *mut FmuComponent {
    let temp_fmu = unsafe {
        INTERNAL_FMU.fmi2Instantiate(
            instanceName,
            fmuType,
            fmuGUID,
            fmuResourceLocation,
            functions,
            visible,
            loggingOn,
        )
    };
    opaque_pointer::raw(FmuComponent {
        internal: temp_fmu,
        test: Box::new(420.0),
    })
}

#[no_mangle]
extern "C" fn fmi2FreeInstance(c: *mut FmuComponent) {
    unsafe {
        INTERNAL_FMU.fmi2FreeInstance(opaque_pointer::mut_object(c).unwrap().internal);
        opaque_pointer::own_back::<FmuComponent>(c).unwrap();
    }
}

#[no_mangle]
extern "C" fn fmi2SetupExperiment(
    c: *mut FmuComponent,
    toleranceDefined: fmi2Boolean,
    tolerance: fmi2Real,
    startTime: fmi2Real,
    stopTimeDefined: fmi2Boolean,
    stopTime: fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetupExperiment(
            opaque_pointer::mut_object(c).unwrap().internal,
            toleranceDefined,
            tolerance,
            startTime,
            stopTimeDefined,
            stopTime,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2EnterInitializationMode(c: *mut FmuComponent) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2EnterInitializationMode(opaque_pointer::mut_object(c).unwrap().internal)
    }
}

#[no_mangle]
extern "C" fn fmi2ExitInitializationMode(c: *mut FmuComponent) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2ExitInitializationMode(opaque_pointer::mut_object(c).unwrap().internal)
    }
}

#[no_mangle]
extern "C" fn fmi2Terminate(c: *mut FmuComponent) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2Terminate(opaque_pointer::mut_object(c).unwrap().internal) }
}

#[no_mangle]
extern "C" fn fmi2Reset(c: *mut FmuComponent) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2Reset(opaque_pointer::mut_object(c).unwrap().internal) }
}

#[no_mangle]
extern "C" fn fmi2GetReal(
    mut c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *mut fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetReal(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetInteger(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *mut fmi2Integer,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetInteger(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetBoolean(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *mut fmi2Boolean,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetBoolean(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetString(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *mut fmi2String,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetString(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetReal(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *const fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetReal(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetInteger(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *const fmi2Integer,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetInteger(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetBoolean(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *const fmi2Boolean,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetBoolean(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetString(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    value: *const fmi2String,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetString(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetFMUstate(c: *mut FmuComponent, FMUstate: *mut fmi2FMUstate) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetFMUstate(opaque_pointer::mut_object(c).unwrap().internal, FMUstate)
    }
}

#[no_mangle]
extern "C" fn fmi2SetFMUstate(c: *mut FmuComponent, FMUstate: fmi2FMUstate) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetFMUstate(opaque_pointer::mut_object(c).unwrap().internal, FMUstate)
    }
}

#[no_mangle]
extern "C" fn fmi2FreeFMUstate(c: *mut FmuComponent, FMUstate: *mut fmi2FMUstate) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2FreeFMUstate(opaque_pointer::mut_object(c).unwrap().internal, FMUstate)
    }
}

#[no_mangle]
extern "C" fn fmi2SerializedFMUstateSize(
    c: *mut FmuComponent,
    FMUstate: fmi2FMUstate,
    size: *mut size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SerializedFMUstateSize(
            opaque_pointer::mut_object(c).unwrap().internal,
            FMUstate,
            size,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SerializeFMUstate(
    c: *mut FmuComponent,
    FMUstate: fmi2FMUstate,
    serializedState: *mut fmi2Byte,
    size: size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SerializeFMUstate(
            opaque_pointer::mut_object(c).unwrap().internal,
            FMUstate,
            serializedState,
            size,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2DeSerializeFMUstate(
    c: *mut FmuComponent,
    serializedState: *const fmi2Byte,
    size: size_t,
    FMUstate: *mut fmi2FMUstate,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2DeSerializeFMUstate(
            opaque_pointer::mut_object(c).unwrap().internal,
            serializedState,
            size,
            FMUstate,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetDirectionalDerivative(
    c: *mut FmuComponent,
    vUnknown_ref: *const fmi2ValueReference,
    nUnknown: size_t,
    vKnown_ref: *const fmi2ValueReference,
    nKnown: size_t,
    dvKnown: *const fmi2Real,
    dvUnknown: *mut fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetDirectionalDerivative(
            opaque_pointer::mut_object(c).unwrap().internal,
            vUnknown_ref,
            nUnknown,
            vKnown_ref,
            nKnown,
            dvKnown,
            dvUnknown,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2EnterEventMode(c: *mut FmuComponent) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2EnterEventMode(opaque_pointer::mut_object(c).unwrap().internal) }
}

#[no_mangle]
extern "C" fn fmi2NewDiscreteStates(
    c: *mut FmuComponent,
    fmi2eventInfo: *mut internal_bindings::fmi2EventInfo,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2NewDiscreteStates(
            opaque_pointer::mut_object(c).unwrap().internal,
            fmi2eventInfo,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2EnterContinuousTimeMode(c: *mut FmuComponent) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2EnterContinuousTimeMode(opaque_pointer::mut_object(c).unwrap().internal)
    }
}

#[no_mangle]
extern "C" fn fmi2CompletedIntegratorStep(
    c: *mut FmuComponent,
    noSetFMUStatePriorToCurrentPoint: fmi2Boolean,
    enterEventMode: *mut fmi2Boolean,
    terminateSimulation: *mut fmi2Boolean,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2CompletedIntegratorStep(
            opaque_pointer::mut_object(c).unwrap().internal,
            noSetFMUStatePriorToCurrentPoint,
            enterEventMode,
            terminateSimulation,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetTime(c: *mut FmuComponent, time: fmi2Real) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2SetTime(opaque_pointer::mut_object(c).unwrap().internal, time) }
}

#[no_mangle]
extern "C" fn fmi2SetContinuousStates(
    c: *mut FmuComponent,
    x: *const fmi2Real,
    nx: size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetContinuousStates(opaque_pointer::mut_object(c).unwrap().internal, x, nx)
    }
}

#[no_mangle]
extern "C" fn fmi2GetDerivatives(
    c: *mut FmuComponent,
    derivatives: *mut fmi2Real,
    nx: size_t,
) -> fmi2Status {
    let fmi_result = unsafe {
        INTERNAL_FMU.fmi2GetDerivatives(
            opaque_pointer::mut_object(c).unwrap().internal,
            derivatives,
            nx,
        )
    };

    lazy_static! {
        static ref model: SimplePlan<TypedFact, Box<dyn TypedOp>, Graph<TypedFact, Box<dyn TypedOp>>> = {
            let mut me_model = Cursor::new(ONNX_BYTES!() as &[u8]);
            tract_onnx::onnx()
                .model_for_read(&mut me_model)
                .unwrap()
                .with_input_fact(0, InferenceFact::dt_shape(f32::datum_type(), tvec!(1, 2)))
                .unwrap()
                .into_optimized()
                .unwrap()
                .into_runnable()
                .unwrap()
        };
    }

    assert!(nx == 2, "fmi2GetDerivatives: nx != 2");

    let derivatives_slice_f64 = unsafe { std::slice::from_raw_parts_mut(derivatives, nx as usize) };
    let derivatives_slice: Vec<f32> = derivatives_slice_f64
        .to_vec()
        .iter()
        .map(|x| x.to_owned() as f32)
        .collect();

    let input: Tensor =
        tract_ndarray::Array::from_shape_vec((1, nx as usize), derivatives_slice.into())
            .unwrap()
            .into();

    let result = model.run(tvec!(input)).unwrap();

    let nn_output: Vec<f64> = result[0]
        .as_slice::<f32>()
        .unwrap()
        .iter()
        .map(|i| i.clone() as f64)
        .collect();

    assert!(nn_output.len() == derivatives_slice_f64.len());

    let mut result_vec = Vec::new();
    for (i, x) in nn_output.iter().enumerate() {
        let bypass = derivatives_slice_f64[i];
        result_vec.push(bypass + x);
    }

    assert!(
        result_vec.len() == nx as usize,
        "result has the wrong shape"
    );
    unsafe {
        use std::ptr::copy_nonoverlapping;
        copy_nonoverlapping(result_vec.as_ptr(), derivatives, nx as usize);
    }

    fmi_result
}

#[no_mangle]
extern "C" fn fmi2GetEventIndicators(
    c: *mut FmuComponent,
    eventIndicators: *mut fmi2Real,
    ni: size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetEventIndicators(
            opaque_pointer::mut_object(c).unwrap().internal,
            eventIndicators,
            ni,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetContinuousStates(
    c: *mut FmuComponent,
    x: *mut fmi2Real,
    nx: size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetContinuousStates(opaque_pointer::mut_object(c).unwrap().internal, x, nx)
    }
}

#[no_mangle]
extern "C" fn fmi2GetNominalsOfContinuousStates(
    c: *mut FmuComponent,
    x_nominal: *mut fmi2Real,
    nx: size_t,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetNominalsOfContinuousStates(
            opaque_pointer::mut_object(c).unwrap().internal,
            x_nominal,
            nx,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2SetRealInputDerivatives(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    order: *const fmi2Integer,
    value: *const fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2SetRealInputDerivatives(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            order,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2GetRealOutputDerivatives(
    c: *mut FmuComponent,
    vr: *const fmi2ValueReference,
    nvr: size_t,
    order: *const fmi2Integer,
    value: *mut fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetRealOutputDerivatives(
            opaque_pointer::mut_object(c).unwrap().internal,
            vr,
            nvr,
            order,
            value,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2DoStep(
    c: *mut FmuComponent,
    currentCommunicationPoint: fmi2Real,
    communicationStepSize: fmi2Real,
    noSetFMUStatePriorToCurrentPoint: fmi2Boolean,
) -> fmi2Status {
    unsafe {
        let internal = opaque_pointer::mut_object(c).unwrap().internal;
        /*         let mut previous_value = [0.0];
        let vr = [352321536];
        let value_offset = [420.0];
        INTERNAL_FMU.fmi2GetReal(
            internal,
            vr.as_ptr(),
            vr.len() as u64,
            previous_value.as_mut_ptr(),
        );
        previous_value[0] += value_offset[0] * communicationStepSize;
        INTERNAL_FMU.fmi2SetReal(
            internal,
            vr.as_ptr(),
            vr.len() as u64,
            previous_value.as_ptr(),
        ); */
        INTERNAL_FMU.fmi2DoStep(
            internal,
            currentCommunicationPoint,
            communicationStepSize,
            noSetFMUStatePriorToCurrentPoint,
        )
    }
}

#[no_mangle]
extern "C" fn fmi2CancelStep(c: *mut FmuComponent) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2CancelStep(opaque_pointer::mut_object(c).unwrap().internal) }
}

#[no_mangle]
extern "C" fn fmi2GetStatus(
    c: *mut FmuComponent,
    s: fmi2StatusKind,
    value: *mut fmi2Status,
) -> fmi2Status {
    unsafe { INTERNAL_FMU.fmi2GetStatus(opaque_pointer::mut_object(c).unwrap().internal, s, value) }
}

#[no_mangle]
extern "C" fn fmi2GetRealStatus(
    c: *mut FmuComponent,
    s: fmi2StatusKind,
    value: *mut fmi2Real,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetRealStatus(opaque_pointer::mut_object(c).unwrap().internal, s, value)
    }
}

#[no_mangle]
extern "C" fn fmi2GetIntegerStatus(
    c: *mut FmuComponent,
    s: fmi2StatusKind,
    value: *mut fmi2Integer,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetIntegerStatus(opaque_pointer::mut_object(c).unwrap().internal, s, value)
    }
}

#[no_mangle]
extern "C" fn fmi2GetBooleanStatus(
    c: *mut FmuComponent,
    s: fmi2StatusKind,
    value: *mut fmi2Boolean,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetBooleanStatus(opaque_pointer::mut_object(c).unwrap().internal, s, value)
    }
}

#[no_mangle]
extern "C" fn fmi2GetStringStatus(
    c: *mut FmuComponent,
    s: fmi2StatusKind,
    value: *mut fmi2String,
) -> fmi2Status {
    unsafe {
        INTERNAL_FMU.fmi2GetStringStatus(opaque_pointer::mut_object(c).unwrap().internal, s, value)
    }
}
