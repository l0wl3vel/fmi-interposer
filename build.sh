#!/bin/sh

FMU_NAME=SpringPendulum1D

RUSTFLAGS="-L /mingw64/lib" cargo build --release --target x86_64-pc-windows-gnu && 
cargo build --release --target x86_64-unknown-linux-gnu && 

unzip -o ${FMU_NAME}.fmu_orig -d ./${FMU_NAME} &&  

cp ${FMU_NAME}/binaries/linux64/${FMU_NAME}.so ${FMU_NAME}/binaries/linux64/${FMU_NAME}_internal.so && 
cp ${FMU_NAME}/binaries/win64/${FMU_NAME}.dll ${FMU_NAME}/binaries/win64/${FMU_NAME}_internal.dll && 


cp target/x86_64-unknown-linux-gnu/release/libfmi_interposer.so ${FMU_NAME}/binaries/linux64/${FMU_NAME}.so && 
cp target/x86_64-pc-windows-gnu/release/fmi_interposer.dll ${FMU_NAME}/binaries/win64/${FMU_NAME}.dll && 

cd ${FMU_NAME} && 
zip -r ../${FMU_NAME}.fmu .